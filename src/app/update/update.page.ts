import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.page.html',
  styleUrls: ['./update.page.scss'],
})
export class UpdatePage implements OnInit {

  formProdutupdate: FormGroup;
  constructor(
    private serviceProduct: ProductService,
    private router: Router
    ) { }

    ngOnInit() {
      this.formProdutupdate = new FormGroup({
        pcantidad: new FormControl(
          1,
          {
            updateOn: 'blur',
            validators: [Validators.required, Validators.min(1)]
          }
        ),
        pcodigo: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required, Validators.minLength(3)]
          }
        ),
        pnombre: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required, Validators.minLength(3)]
          }
        ),
        ppeso: new FormControl(
          1,
          {
            updateOn: 'blur',
            validators:[Validators.required, Validators.min(1)]
          }
        ),
        pfecha_caducidad: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required]
          }
        ),
        pdescripcion: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required, Validators.minLength(30)]
          }
        )
      });
    }
    updateproduct(){
      if(!this.formProdutupdate.valid){
        return;
      }
      this.serviceProduct.addProduct(
        this.formProdutupdate.value.pprecio,
        this.formProdutupdate.value.pcantidad, 
        this.formProdutupdate.value.pcodigo,
        this.formProdutupdate.value.pnombre,
        this.formProdutupdate.value.ppeso,
        this.formProdutupdate.value.pfecha_caducidad,
        this.formProdutupdate.value.pdescripcion
      );
      this.formProdutupdate.reset();
      this.router.navigate(['/products']);
    }
  
  }