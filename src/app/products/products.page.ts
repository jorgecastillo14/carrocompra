import { Component, Input, OnInit } from '@angular/core';
import { ProductService } from './product.service';
import { proveedor, productos } from './products.model';

@Component({
  selector: 'page-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  @Input() nombre: string;
  products: productos[];
  constructor(private productServices: ProductService) { }

  ngOnInit() {

  }
  ionViewWillEnter(){
    console.log("Se obtuvo la lista");
    this.products = this.productServices.getAll();
  }
}
