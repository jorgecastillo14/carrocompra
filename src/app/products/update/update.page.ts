import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../product.service';
import { productos, proveedor } from '../products.model';


@Component({
  selector: 'app-update',
  templateUrl: './update.page.html',
  styleUrls: ['./update.page.scss'],
})
export class UpdatePage implements OnInit {
  product: productos;
  formProdutupdate: FormGroup;
  constructor(
    private activeRouter: ActivatedRoute,
    private serviceProduct: ProductService,
     
    private router: Router
    ) { }
    private proveedore: proveedor[] = [
      {
          nombre: "Intcomex",
          cedula: 11111,
          direccion: "San Jose",
          numero: 8888,
          correo: "www@deee.com",
          codigo: 333
      },
      {
          nombre: "Intcomex",
          cedula: 11111,
          direccion: "San Jose",
          numero: 8888,
          correo: "www@deee.com",
          codigo: 333
      }
    ];
    private productos:productos [] = [
      {
        proveedor: this.proveedore,
        precio:888,
        candidad: 12,
        codigo: 12,
        nombre: "Tarjeta Madre",
        peso: 111,
        fecha_caducidad: "11-11-1",
        descripcion: "Tarjeta madre socket AM4"
      },
      {
        proveedor: this.proveedore,
        precio:888,
        candidad: 12,
        codigo: 11,
        nombre: "Tarjeta Madre",
        peso: 111,
        fecha_caducidad: "11-11-1",
        descripcion: "Tarjeta madre socket Intel 1151"
      }
    ];

    ngOnInit() {
     
      this.formProdutupdate = new FormGroup({
        pcantidad: new FormControl(
          1,
          {
            updateOn: 'blur',
            validators: [Validators.required, Validators.min(1)]
          }
        ),
        pcodigo: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required, Validators.minLength(3)]
          }
        ),
        pnombre: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required, Validators.minLength(3)]
          }
        ),
        ppeso: new FormControl(
          1,
          {
            updateOn: 'blur',
            validators:[Validators.required, Validators.min(1)]
          }
        ),
        pfecha_caducidad: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required]
          }
        ),
        pdescripcion: new FormControl(
          null,
          {
            updateOn: 'blur',
            validators:[Validators.required, Validators.minLength(10)]
          }
        )
      });
  
      
    
    }
    
       
    updateproduct(){
      if(!this.formProdutupdate.valid){
        return;
      }
      this.serviceProduct.updateproduct(
        this.formProdutupdate.value.pprecio,
        this.formProdutupdate.value.pcantidad, 
        this.formProdutupdate.value.pcodigo,
        this.formProdutupdate.value.pnombre,
        this.formProdutupdate.value.ppeso,
        this.formProdutupdate.value.pfecha_caducidad,
        this.formProdutupdate.value.pdescripcion
      );
      this.formProdutupdate.reset();
      this.router.navigate(['/products']);
    }
  
  }