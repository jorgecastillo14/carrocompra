import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { productos, proveedor } from './products.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private proveedore: proveedor[] = [
    {
        nombre: "Intcomex",
        cedula: 11111,
        direccion: "San Jose",
        numero: 8888,
        correo: "www@deee.com",
        codigo: 333
    },
    {
        nombre: "Intcomex",
        cedula: 11111,
        direccion: "San Jose",
        numero: 8888,
        correo: "www@deee.com",
        codigo: 333
    }
  ];
  private productos:productos [] = [
    {
      proveedor: this.proveedore,
      precio:888,
      candidad: 12,
      codigo: 12,
      nombre: "Tarjeta Madre",
      peso: 111,
      fecha_caducidad: "11-11-1",
      descripcion: "Tarjeta madre socket AM4"
    },
    {
      proveedor: this.proveedore,
      precio:888,
      candidad: 12,
      codigo: 11,
      nombre: "Tarjeta Madre",
      peso: 111,
      fecha_caducidad: "11-11-1",
      descripcion: "Tarjeta madre socket Intel 1151"
    }
  ];
    constructor(
        private http: HttpClient
      ) { }
  getAll(){
    return [...this.productos];
  }
  getProduct(productId: number){
    return {
      ...this.productos.find(
        product => {
          return product.codigo === productId;
        }
      )
    };
  }
  deleteProduct(productId: number){
    this.productos = this.productos.filter(
      product => {
        return product.codigo !== productId;
      }
    );
  }
  updateproduct (pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pfecha_caducidad: string,
    pdescripcion: string){

      const product: productos = {
        proveedor: this.proveedore,
        precio: pprecio,
        candidad: pcantidad,
        codigo: pcodigo,
        nombre: pnombre,
        peso: ppeso,
        fecha_caducidad: pfecha_caducidad,
        descripcion: pdescripcion
      }
      this.http.post('https://carrocompras-18be2.firebaseio.com/addProduct.json',
             { 
               ...product, 
               id:null
             }).subscribe(() => {
              console.log('entro');
              });
             this.productos.push(product);
  }

   

    

  addProduct(pprecio: number,
    pcantidad: number,
    pcodigo: number,
    pnombre: string,
    ppeso: number,
    pfecha_caducidad: string,
    pdescripcion: string){

      const product: productos = {
        proveedor: this.proveedore,
        precio: pprecio,
        candidad: pcantidad,
        codigo: pcodigo,
        nombre: pnombre,
        peso: ppeso,
        fecha_caducidad: pfecha_caducidad,
        descripcion: pdescripcion
      }
      this.http.post('https://carrocompras-18be2.firebaseio.com/addProduct.json',
             { 
               ...product, 
               id:null
             }).subscribe(() => {
              console.log('entro');
              });
             this.productos.push(product);
  }
  
    
}
