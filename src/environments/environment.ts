// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
  apiKey: "AIzaSyAkVDynx9Hk34z-UIBXdNjE7pssBr0l-MQ",
    authDomain: "carrocompras-18be2.firebaseapp.com",
    databaseURL: "https://carrocompras-18be2.firebaseio.com",
    projectId: "carrocompras-18be2",
    storageBucket: "carrocompras-18be2.appspot.com",
    messagingSenderId: "213952977831",
    appId: "1:213952977831:web:c2c286bfd37e2858e7f482",
    measurementId: "G-GBS3G6RN4Q"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
